/*
 * Extruder.j
 * Authory
 *
 * Created by Mathieu Monney on October 11, 2014.
 * Copyright 2014, Xwaves All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>
@import "Ratatosk/Ratatosk.j"

@implementation Extruder : WLRemoteObject
{
    int         pk                @accessors;
    double		target_temperature @accessors;
    double		temperature		  @accessors;
}

+ (CPArray)remoteProperties
{
    return [
        ['pk', 'id'],
        ['target_temperature', 'target_temperature'],
        ['temperature', 'temperature']
    ];
}

- (CPString)remotePath
{
    return @"extruder/" +  pk;
}



@end
