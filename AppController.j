    /*
 * AppController.j
 * Authory
 *
 * Created by Mathieu Monney on September 18, 2014.
 * Copyright 2014, Xwaves All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>
@import "Ratatosk/Ratatosk.j"
@import "PrinterControlViewController.j"
@import "TemperatureViewController.j"
@import "PrinterController.j"
@import "GCodeView.j"
@import "CPDictionary+URL.j"

@implementation AppController : CPObject
{
    @outlet CPWindow    theWindow;
    @outlet PrinterControlViewController printerControllerViewController @accessors(readonly);
    @outlet TemperatureViewController temperatureViewController @accessors(readonly);

    @outlet CPTabViewItem printTabViewItem;
    @outlet CPTabViewItem fileTabView;
}

- (void)setupDefaults
{
    // load the default values for the user defaults
    var userDefaultsValuesPath=[[CPBundle mainBundle] pathForResource:@"defaultSettings.plist"];
    var userDefaultsValuesDict=[CPDictionary dictionaryWithContentsOfURL:userDefaultsValuesPath];
 
    // set them in the standard user defaults
    [[CPUserDefaults standardUserDefaults] registerDefaults:userDefaultsValuesDict];

    // Set the initial values in the shared user defaults controller
    [[CPUserDefaultsController sharedUserDefaultsController] setInitialValues:userDefaultsValuesDict];
}

- (float)splitView:(CPSplitView)splitView constrainSplitPosition:(float)proposedPosition ofSubviewAt:(CPInteger)dividerIndex {
    var size = [[theWindow platformWindow] contentBounds].size;

    if(size.width - proposedPosition>=560) {
         return proposedPosition;
    } else {
        return size.width -560;
    }
}

- (void)applicationDidFinishLaunching:(CPNotification)aNotification
{
    // This is called when the application is done loading.
    
   

    [WLRemoteLink setDefaultBaseURL:[[CPBundle mainBundle] objectForInfoDictionaryKey:@"apiBaseURL"]];
    [[WLRemoteLink sharedRemoteLink] setSaveActionType:WLRemoteActionPutType];

    [[PrinterController instance] currentPrinter];
}

-(void) setupViews {
	var v = [printerControllerViewController view];
	[printTabViewItem setView:v];

	/*var bounds = [rightTabView bounds];
	console.log(rightTabView);
	[v setBounds:bounds];*/
}

- (void)awakeFromCib
{
    [self setupDefaults];

    // In this case, we want the window from Cib to become our full browser window
    [theWindow setFullPlatformWindow:YES];

 	[self setupViews];
}

@end
