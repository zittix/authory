/*
 * Printer.j
 * Authory
 *
 * Created by Mathieu Monney on September 21, 2014.
 * Copyright 2014, Xwaves All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>
@import "Ratatosk/Ratatosk.j"

@implementation Printer : WLRemoteObject
{
    int             pk                @accessors;
    CPString        name              @accessors;
    CPDictionary    extruders         @accessors;
}

+ (CPArray)remoteProperties
{
    return [
        ['pk', 'id'],
        ['name', 'name']
    ];
}

- (CPString)remotePath
{
    return @"printer/" +  pk;
}



@end
