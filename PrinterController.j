/*
 * PrinterController.j
 * Authory
 *
 * Created by Mathieu Monney on September 21, 2014.
 * Copyright 2014, Xwaves All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>
@import "Printer.j"
@import "SCSocket.j"

var inst = null;

@implementation PrinterController : CPViewController
{
	Printer currentPrinter @accessors(readonly);
    SCSocket webSocket @accessors(readonly);
}

+(PrinterController)instance {
    if(!inst) {
        inst = [[PrinterController alloc] init];
    }

    return inst;
}

-(void)private_init {
    var url = [[CPBundle mainBundle] objectForInfoDictionaryKey:@"apiBaseURL"]+'/ws';

    webSocket = [[SCSocket alloc] initWithURL:[CPURL URLWithString:url] delegate:self];
}

-(id) init {
    self = [super init];
    if(self) {
        inst = self;
        [self private_init];
    }

    return self;
}

-(id) initWithCoder:(CPCoder)c {
    self = [super initWithCoder:c];
    if(self) {
        inst = self;
        [self private_init];
    }

    return self;
}

-(void)socket:(SCSocket)socket didReceiveMessage:(id)object {
    if(object.data && object.data.temperature) {
        [[[[CPApplication sharedApplication] delegate] temperatureViewController] addTemperature:object.data.temperature];
    }
}

-(Printer) currentPrinter {
    if(!currentPrinter) {
        [WLRemoteAction schedule:WLRemoteActionGetType path:'printer' delegate:self message:"Loading printer info"];
    }

    return currentPrinter;
}


-(void) moveHead:(CPString)axis distance:(double)amount speed:(double)speed {
    [self sendGCode:@"G91"];
    if(axis==@"Z")
        [self sendGCode:[CPString stringWithFormat:@"G1 %@%1.1f F%1.0f",axis,amount,speed]];
    else
        [self sendGCode:[CPString stringWithFormat:@"G1 %@%1.1f F%1.0f",axis,amount,speed]];
    [self sendGCode:@"G90"];
}

- (void)sendGCode:(CPString)command {
    var action = [[WLRemoteAction alloc] initWithType:WLRemoteActionPostType path:[currentPrinter remotePath] + @"/gcode" delegate:nil message:@"Sending GCode command"];
    [action setPayload:command];
    [action schedule];
}

- (void)sendGCodes:(CPArray)commands {
    //Transform as string
    for(var i = 0; i<[commands count];i++) {
        [self sendGCode:[commands objectAtIndex:i]];
    }
}

- (void)remoteActionDidFinish:(WLRemoteAction)anAction
{
    var result = [anAction result];
    if (result) {
        currentPrinter = [[Printer alloc] initWithJson:result];
    } else {
        //FIXME throw error
    }
}

- (void)remoteActionDidFail:(WLRemoteAction)anAction
{

}

@end
