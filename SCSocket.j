@import <Foundation/CPObject.j>
@import "js/sockjs-0.3.4.js"

@global io;

@implementation SCSocket : CPObject
{
    JSObject socket;
    id delegate;
}

- (id)initWithURL:(CPURL)aURL delegate:aDelegate
{
    self = [super init];
    if (self)
    {
        socket = new SockJS([aURL absoluteString], null, {debug:true    });
        delegate = aDelegate;
        if ([delegate respondsToSelector:@selector(socketDidConnect:)]) {
            socket.onopen = function() {
                [delegate socketDidConnect:self]; [[CPRunLoop currentRunLoop] limitDateForMode:CPDefaultRunLoopMode];
            };
        }
        if ([delegate respondsToSelector:@selector(socketDidClose:)]) {
            socket.onclose = function() {
                [delegate socketDidClose:self]; [[CPRunLoop currentRunLoop] limitDateForMode:CPDefaultRunLoopMode];
            };
        }

        if ([delegate respondsToSelector:@selector(socket:didReceiveMessage:)]) {
            socket.onmessage = function(e) {
                [delegate socket:self didReceiveMessage:e]; [[CPRunLoop currentRunLoop] limitDateForMode:CPDefaultRunLoopMode];
            };
        }

    }
    return self;
}


- (void)connect
{
    socket.connect();
}

- (void)close
{
    if (socket) 
    {
        socket.disconnect();
    }
}

- (void)sendMessage:(CPString)aMessage
{
    socket.send(aMessage);
}

@end