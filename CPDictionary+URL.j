@import <Foundation/Foundation.j>


@implementation CPDictionary(URLAddition)

 /*!
    Sends a synchronous request to given URL and tries to parse the output as
    property list having a dictionary as root element.
    @param aURL target URL
    @return the initialized dictionary or nil on failure
    
    You can use this method to quickly load some data from server:
    <pre>var d = [CPDictionary dictionaryWithContentsOfURL:@"data/Info.plist"];
if (d)
    var version = [d objectForKey:@"version"];</pre>
    You should always check returned value, as various network problems might result in
    dictionaryWithContentsOfURL returning nil.
*/
+ (id)dictionaryWithContentsOfURL:(CPURL)aURL
{
    var plist = [CPURLConnection sendSynchronousRequest:[CPURLRequest requestWithURL:aURL] returningResponse:nil];
    if (plist == nil)
    {
        return nil;
    }
    var po = [plist plistObject];
    if ([po isKindOfClass:CPDictionary])
        return po;
    else
        return nil;
}


/*!
     Initializes the dictionary with the contents of another dictionary.
     @param aDictionary the dictionary to copy key-value pairs from
     @return the initialized dictionary
@@ -165,6 +193,18 @@
 }
 
 /*!
    Sends a synchronous request to given URL and tries to parse the output as
    property list having a dictionary as root element.
    @see [CPDictionary dictionaryWithContentsOfURL:]
    @param aURL target URL
    @return the initialized dictionary or nil on failure
*/
- (id)initWithContentsOfURL:(CPURL)aURL
{
    return [CPDictionary dictionaryWithContentsOfURL:aURL];
}

@end