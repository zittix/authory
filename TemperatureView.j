/*
 * TemperatureView.j
 * Authory
 *
 * Created by Mathieu Monney on October 3rd, 2014.
 * Copyright 2014, Xwaves All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>
@import "js/smoothie.js"

@global SmoothieChart;
var i=0;
@implementation TemperatureView : CPView {
    var smoothie;
    var series;
}

-(void)internalInit {
   
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self internalInit];
    }
    
    return self;
}

-(void)addMeasure:(float)m {
    series.append(new Date().getTime(), m);
}

-(void)drawRect:(CGRect)r {
    if(smoothie == null) {
        smoothie = new SmoothieChart({
            millisPerPixel:100,
            grid:{
                strokeStyle:'rgba(119,119,119,0.46)',
                millisPerLine:6000,
                verticalSections:7
            },
            labels:{
                disabled:true
            },
            timestampFormatter: SmoothieChart.timeFormatter,
            yAxisLabelFormatter: function(value,precision) {
                return parseFloat(value).toFixed(0)+" °C";
            },
            maxValueScale:1.1
        });


        smoothie.streamTo(_DOMElement.children[0],1000);

        series = new TimeSeries();
        series.append(new Date().getTime(), 0);

        smoothie.addTimeSeries(series, {lineWidth:2,strokeStyle:'#ff0000'});

    }

 
}

- (id)initWithCoder:(CPCoder)aCoder
{
    self = [super initWithCoder:aCoder];
    if (self) {
        [self internalInit];
    }
    
    return self;
}


- (BOOL)acceptsFirstResponder
{
    return YES;
}
- (BOOL)becomeFirstResponder
{
    return YES;
}

-(void)mouseDown:(CPEvent)theEvent {

}
-(void)mouseDragged:(CPEvent)theEvent {
   
}


@end
