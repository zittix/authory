/*
 * PrinterControlViewController.j
 * Authory
 *
 * Created by Mathieu Monney on September 18, 2014.
 * Copyright 2014, Xwaves All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>
@import "PrinterController.j"

@implementation PrinterControlViewController : CPViewController
{
    double travelSpeed @accessors;
}

-(IBAction)xMoveAction:(id) sender {
	switch ([sender selectedSegment]) {
        case 0:
            [[PrinterController instance] moveHead:@"X" distance:-100 speed:travelSpeed];
            break;
        case 1:
            [[PrinterController instance] moveHead:@"X" distance:-10 speed:travelSpeed];
            break;
        case 2:
            [[PrinterController instance] moveHead:@"X" distance:-1 speed:travelSpeed];
            break;
        case 3:
            [[PrinterController instance] moveHead:@"X" distance:-0.1 speed:travelSpeed];
            break;
        case 4:
            [[PrinterController instance] moveHead:@"X" distance:0.1 speed:travelSpeed];
            break;
        case 5:
            [[PrinterController instance] moveHead:@"X" distance:1 speed:travelSpeed];
            break;
        case 6:
            [[PrinterController instance] moveHead:@"X" distance:10 speed:travelSpeed];
            break;
        case 7:
            [[PrinterController instance] moveHead:@"X" distance:100 speed:travelSpeed];
            break;
    }
}

-(IBAction)yMoveAction:(id) sender {
	switch ([sender selectedSegment]) {
        case 0:
            [[PrinterController instance] moveHead:@"Y" distance:-100 speed:travelSpeed];
            break;
        case 1:
            [[PrinterController instance] moveHead:@"Y" distance:-10 speed:travelSpeed];
            break;
        case 2:
            [[PrinterController instance] moveHead:@"Y" distance:-1 speed:travelSpeed];
            break;
        case 3:
            [[PrinterController instance] moveHead:@"Y" distance:-0.1 speed:travelSpeed];
            break;
        case 4:
            [[PrinterController instance] moveHead:@"Y" distance:0.1 speed:travelSpeed];
            break;
        case 5:
            [[PrinterController instance] moveHead:@"Y" distance:1 speed:travelSpeed];
            break;
        case 6:
            [[PrinterController instance] moveHead:@"Y" distance:10 speed:travelSpeed];
            break;
        case 7:
            [[PrinterController instance] moveHead:@"Y" distance:100 speed:travelSpeed];
            break;
    }
}

-(IBAction)zMoveAction:(id) sender {
	switch ([sender selectedSegment]) {
        case 0:
            [[PrinterController instance] moveHead:@"Z" distance:-100 speed:travelSpeed];
            break;
        case 1:
            [[PrinterController instance] moveHead:@"Z" distance:-10 speed:travelSpeed];
            break;
        case 2:
            [[PrinterController instance] moveHead:@"Z" distance:-1 speed:travelSpeed];
            break;
        case 3:
            [[PrinterController instance] moveHead:@"Z" distance:-0.1 speed:travelSpeed];
            break;
        case 4:
            [[PrinterController instance] moveHead:@"Z" distance:0.1 speed:travelSpeed];
            break;
        case 5:
            [[PrinterController instance] moveHead:@"Z" distance:1 speed:travelSpeed];
            break;
        case 6:
            [[PrinterController instance] moveHead:@"Z" distance:10 speed:travelSpeed];
            break;
        case 7:
            [[PrinterController instance] moveHead:@"Z" distance:100 speed:travelSpeed];
            break;
    }
}

- (void)awakeFromCib
{    
    [self bind:@"travelSpeed" toObject:[CPUserDefaultsController sharedUserDefaultsController] withKeyPath:@"values.travelSpeed" options:@{CPContinuouslyUpdatesValueBindingOption: YES}];
}

@end
