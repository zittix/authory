/*
 * GCodeView.j
 * Authory
 *
 * Created by Mathieu Monney on October 02, 2014.
 * Copyright 2014, Xwaves All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/AppKit.j>
@import "Ratatosk/Ratatosk.j"
@import "PrinterControlViewController.j"
@import "PrinterController.j"
@import "js/three.min.js"
@import "js/webGLDetector.js"

@global THREE;
@global WebGLDetector;

@implementation GCodeView : CPView
{
    var camera;
    var renderer;
    var scene;
    var cube;
}

-(void)render {
    requestAnimationFrame(function() {
        [self render];
    });

    cube.rotation.x += 0.1;
    cube.rotation.y += 0.1;

    renderer.render(scene, camera);
}

-(void)layoutSubviews {
    [super layoutSubviews];
    var size = [self bounds].size;

    renderer.setSize(size.width, size.height);
    camera.aspect = size.width/size.height;
    camera.updateProjectionMatrix();

}

-(void)internalInit {
    scene = new THREE.Scene();

    var size = [self bounds].size;

    camera = new THREE.PerspectiveCamera(45, size.width/size.height, 0.1, 1000);

    renderer = WebGLDetector.webgl? new THREE.WebGLRenderer(): new THREE.CanvasRenderer();

    renderer.setSize(size.width, size.height);
    _DOMElement.appendChild(renderer.domElement);

    var geometry = new THREE.BoxGeometry(1,1,1);
    var material = new THREE.MeshLambertMaterial({color: 0x00ff00});

    cube = new THREE.Mesh(geometry, material);
    scene.add(cube);

    // add subtle ambient lighting
    var ambientLight = new THREE.AmbientLight(0x222222);
    scene.add(ambientLight);

    // directional lighting
    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(1, 1, 1).normalize();
    scene.add(directionalLight);
    camera.position.z = 5;

    [self render];
}

- (id)initWithFrame:(CGRect)aFrame
{
    self = [super initWithFrame:aFrame];

    if (self)
    {
        [self internalInit];
    }

    return self;
}

- (id)initWithCoder:(CPCoder)aCoder
{
    self = [super initWithCoder:aCoder];

    if (self)
    {
        [self internalInit];
    }

    return self;
}


@end
